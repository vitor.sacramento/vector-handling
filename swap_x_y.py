from osgeo import ogr
from tkinter.filedialog import askopenfilename
import os


in_shape = askopenfilename(
    title='Selecione o shape de entrada',
    filetypes=[('Shapefile','*.shp')])

# open the input shapefile
in_ds = ogr.Open(in_shape)

# get the layer and geometry
in_lyr = in_ds.GetLayer()
in_feat = in_lyr.GetNextFeature()
in_geom = in_feat.GetGeometryRef()

# create a new output shapefile
out_ds = ogr.GetDriverByName('ESRI Shapefile').CreateDataSource(os.path.join(in_shape,os.path.splitext(in_shape)[0])+"_SWAP.shp")

# create a new layer with the same name and spatial reference system as the input
out_lyr = out_ds.CreateLayer(in_lyr.GetName(), in_lyr.GetSpatialRef())

# create a new feature with the same fields as the input
out_feat = ogr.Feature(out_lyr.GetLayerDefn())
out_feat.SetFrom(in_feat)

# create a new geometry with swapped coordinates
out_geom = ogr.Geometry(ogr.wkbMultiPolygon)
for i in range(in_geom.GetGeometryCount()):
    polygon = in_geom.GetGeometryRef(i)
    new_polygon = ogr.Geometry(ogr.wkbPolygon)
    ring = polygon.GetGeometryRef(0)
    if ring is not None: # check if ring is not None
        new_ring = ogr.Geometry(ogr.wkbLinearRing)
        for j in range(ring.GetPointCount()):
            x, y, z = ring.GetPoint(j)
            new_ring.AddPoint(y, x, z)
        new_polygon.AddGeometry(new_ring)
        out_geom.AddGeometry(new_polygon)

# set the geometry of the output feature and add it to the layer
out_feat.SetGeometry(out_geom)
out_lyr.CreateFeature(out_feat)

# close the shapefiles
in_ds = None
out_ds = None