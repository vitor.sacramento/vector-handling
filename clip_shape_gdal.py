from osgeo import gdal, ogr
import os

input_shapefile = "/home/vitor/Downloads/FUSOS_UTM.shp"
clip_shapefile = "/home/vitor/Downloads/shapes_postgis/aprt_limite_bat.shp"
output_shapefile = "/home/vitor/Downloads/shapes_postgis/fusos_clip.shp"

# Open input and clip shapefiles
input_ds = ogr.Open(input_shapefile)
input_layer = input_ds.GetLayer()
clip_ds = ogr.Open(clip_shapefile)
clip_layer = clip_ds.GetLayer()

# Create the output shapefile
driver = ogr.GetDriverByName("ESRI Shapefile")
if os.path.exists(output_shapefile):
    driver.DeleteDataSource(output_shapefile)
output_ds = driver.CreateDataSource(output_shapefile)
output_layer = output_ds.CreateLayer("output_layer", geom_type=ogr.wkbPolygon)

# Perform the clip
for clip_feature in clip_layer:
    clip_geometry = clip_feature.GetGeometryRef()
    input_layer.SetSpatialFilter(clip_geometry)
    for input_feature in input_layer:
        input_geometry = input_feature.GetGeometryRef()
        intersecting_geometry = input_geometry.Intersection(clip_geometry)
        output_feature = ogr.Feature(output_layer.GetLayerDefn())
        output_feature.SetGeometry(intersecting_geometry)
        output_layer.CreateFeature(output_feature)

print("Shapefile clipped successfully.")