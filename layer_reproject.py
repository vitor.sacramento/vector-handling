import geopandas as gpd
from tkinter.filedialog import askdirectory
import os

input_directory = askdirectory(title='Select the layers directory')

input_files = os.listdir(input_directory)

shapefiles = []

for file in input_files:
    if file.endswith('.shp'):
        shapefiles.append(os.path.join(input_directory,file))

for shape in shapefiles:
    gdf = gpd.read_file(shape)
    gdf = gdf.to_crs(crs='EPSG:4326')
    gdf.to_file(os.path.join(input_directory,shape))