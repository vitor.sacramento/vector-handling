from tkinter import *
from tkinter.filedialog import askopenfilenames
import os
import subprocess
from osgeo import ogr

# get a list of all the supported vector formats by GDAL
supported_drivers = ogr.GetDriverCount()
driver_list = []
for i in range(supported_drivers):
    driver = ogr.GetDriver(i)
    if driver:
        driver_name = driver.GetName()
        driver_list.append(driver_name)
driver_list.sort()

# create the GUI window
root = Tk()
root.title("File Converter")
root.geometry("300x500")

# label for the listbox
label = Label(root, text="Select output format:")
label.pack()

# create a scrollbar
scrollbar = Scrollbar(root)
scrollbar.pack(side=RIGHT, fill=Y)

# create the listbox
listbox = Listbox(root, yscrollcommand=scrollbar.set)
for driver in driver_list:
    listbox.insert(END, driver)
listbox.pack(side=LEFT, fill=BOTH)

# link the scrollbar to the listbox
scrollbar.config(command=listbox.yview)

# bind the mouse wheel event to the listbox
listbox.bind("<MouseWheel>", lambda event: listbox.yview_scroll(int(-1*(event.delta/120)), "units"))

# button for selecting the input files
button = Button(root, text="Select files", command=lambda: select_files())
button.pack()

# function for selecting the input files using a file dialog
def select_files():
    global files_to_convert
    files_to_convert = askopenfilenames(title='Select the files to convert')
    convert_files()

# function for converting the selected files
def convert_files():
    driver = listbox.get(ACTIVE)
    for file in files_to_convert:
        file_name, file_ext = os.path.splitext(file)
        output_file = os.path.splitext(file_name)[0]
        if driver == 'ESRI Shapefile':
            command = ['ogr2ogr', '-f', driver, output_file+'.shp', file, os.path.basename(os.path.splitext(file)[0])]
        else:
            command = ['ogr2ogr', '-f', driver, output_file, file, os.path.basename(os.path.splitext(file)[0])]
        print(command)
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        if process.returncode != 0:
            print(f"An error occurred: {stderr.decode('utf-8')}")
        else:
            print(f"ogr2ogr command succeeded: {stdout.decode('utf-8')}")

# start the GUI mainloop
root.mainloop()