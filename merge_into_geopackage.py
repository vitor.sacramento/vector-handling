import geopandas as gpd
from tkinter.filedialog import askopenfilenames
from tkinter.simpledialog import askstring
import os

# List all the vector files you want to merge
file_list = askopenfilenames(title='Selecione as camadas de entrada')
output_name = askstring(prompt='Selecione o nome do arquivo de saída',title=None)

# Create an empty geopandas dataframe to hold the merged data
merged_data = gpd.GeoDataFrame()

# Loop through the file list, read each file and append its data to the merged dataframe
for file in file_list:
    data = gpd.read_file(file)
    data = data.to_crs(epsg='4326')
    merged_data = gpd.concat([merged_data, data], ignore_index=True)

# Save the merged data to a single geopackage file
output_file = os.path.join(os.path.dirname(file_list[0]), output_name)
merged_data.to_file(output_file, driver='GPKG')