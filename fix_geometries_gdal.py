from osgeo import ogr
from tkinter.filedialog import askopenfilenames
import os

# Input file path
input_files = ['/home/vitor/Documentos/test_lasso.shp']

for input_file in input_files:
    # Open the input layer file
    input_layer = ogr.Open(input_file)

    # Get the input layer's geometry type and spatial reference
    layer = input_layer.GetLayer()
    layer_defn = layer.GetLayerDefn()

    geom_type = layer_defn.GetGeomType()
    spatial_ref = layer.GetSpatialRef()

    # Output file path
    output_file = os.path.basename(input_file)+"teste.shp"

    # Create a new output layer file with the same driver as the input file
    driver_name = input_layer.GetDriver().GetName()
    output_driver = ogr.GetDriverByName(driver_name)
    if output_driver.GetName() == "ESRI Shapefile":
        if os.path.exists(output_file):
            output_driver.DeleteDataSource(output_file)
        output_layer = output_driver.CreateDataSource(output_file).CreateLayer("cleaned_layer", spatial_ref, geom_type)
    else:
        output_layer = output_driver.CreateDataSource(output_file, options=["RFC7946=YES"]).CreateLayer("cleaned_layer", spatial_ref, geom_type)

    # Loop through the features in the input layer, perform a buffer of 0, and add them to the output layer if their geometry is valid

    input_feature = layer.GetNextFeature()
    
    while input_feature:
        input_geom = input_feature.GetGeometryRef()
        if input_geom is not None:  # Add a check for valid geometry
            buffer_geom = input_geom.Buffer(0)
            if buffer_geom.IsValid():
                output_feature = ogr.Feature(output_layer.GetLayerDefn())
                output_feature.SetGeometry(buffer_geom)
                output_layer.CreateFeature(output_feature)
        input_feature = layer.GetNextFeature()

    # Clean up
    input_layer = None
    output_layer = None