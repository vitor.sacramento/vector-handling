import geopandas as gpd

# Read the file geodatabase using geopandas
gdf = gpd.read_file("/home/vitor/Documentos/Mapbiomas_tiles.gdb")

# Loop through the feature classes in the file geodatabase
for feature in gdf.iterrows():
    # Get the name of the feature class
    name = feature[1].name
    # Export the feature class to a shapefile
    feature[1].to_file("/home/vitor/Documentos/Mapbiomas_por_estado_Shapes{}.shp".format(name))