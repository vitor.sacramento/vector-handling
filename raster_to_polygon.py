import rasterio
from rasterio.features import geometry_mask
import numpy as np
from numba import jit
from tkinter.filedialog import askopenfilename
import os

# Set the paths to your input and output files
raster_file = askopenfilename(title='Selecione o raster de entrada')

output_polygons = os.path.splitext(raster_file)[0]+'.gpkg'

# Load the raster data using rasterio
with rasterio.open(raster_file) as src:
    # Read the raster data as a numpy array
    data = src.read(1)
    # Get the raster transform
    transform = src.transform

# Define a function to generate polygons using numba
@jit
def generate_polygons(data):
    # Create a mask from the data
    mask = geometry_mask(data, transform=transform, invert=True)
    # Generate the polygons from the mask
    polygons = []
    for geom, val in rasterio.features.shapes(mask, transform=transform):
        polygons.append(geom)
    return polygons

# Generate the polygons using the GPU-accelerated function
polygons = generate_polygons(data)

# Write the polygons to the output GeoPackage file using GDAL
with rasterio.open(output_polygons, 'w', driver='GPKG') as dst:
    dst.write(polygons, 1)