import shapefile

def are_points_collinear(p1, p2, p3):
    """
    Check if three points are collinear.
    """
    x1, y1 = p1
    x2, y2 = p2
    x3, y3 = p3
    return (y2 - y1) * (x3 - x1) == (y3 - y1) * (x2 - x1)

def remove_middle_collinear_point(points):
    """
    Remove the middle collinear point if found in a list of points.
    """
    if len(points) < 3:
        return points

    cleaned_points = [points[0], points[1]]  # Add the first two points

    for i in range(2, len(points)):
        p1, p2, p3 = cleaned_points[-2], cleaned_points[-1], points[i]
        if not are_points_collinear(p1, p2, p3):
            cleaned_points.append(p3)

    return cleaned_points

def read_shapefile_points(shapefile_path):
    """
    Read points from a shapefile and return a list of polygons, each represented by a list of tuples (x, y).
    """
    sf = shapefile.Reader(shapefile_path)
    shapes = sf.shapes()

    polygons = []
    for shape in shapes:
        points = shape.points
        polygons.append(points)

    return polygons

def clean_shapefile(shapefile_path):
    """
    Read a shapefile, repeatedly remove the middle collinear points from each polygon until none are left,
    and write the cleaned shapefile.
    """
    sf = shapefile.Reader(shapefile_path)
    shapes = sf.shapes()
    fields = sf.fields[1:]  # Exclude the deletion flag field

    cleaned_shapes = []
    for shape in shapes:
        points = shape.points
        cleaned_points = remove_middle_collinear_point(points)
        cleaned_shape = shapefile.Shape(shape.shapeType, cleaned_points)
        cleaned_shapes.append(cleaned_shape)

    while True:
        has_collinear_points = False
        updated_shapes = []

        for cleaned_shape in cleaned_shapes:
            cleaned_points = cleaned_shape.points
            new_cleaned_points = remove_middle_collinear_point(cleaned_points)

            if len(new_cleaned_points) < len(cleaned_points):
                has_collinear_points = True

            cleaned_shape.points = new_cleaned_points
            updated_shapes.append(cleaned_shape)

        cleaned_shapes = updated_shapes

        if not has_collinear_points:
            break

    cleaned_sf = shapefile.Writer(target="/home/vitor/Downloads/collinear/shape_collinear_simple", shapeType=sf.shapeType)
    cleaned_sf.fields = fields  # Set the attribute table fields

    for cleaned_shape in cleaned_shapes:
        cleaned_sf.record()  # Add an empty record for each shape
        cleaned_sf.shape(cleaned_shape)  # Add the cleaned shape

    cleaned_sf.save('/home/vitor/Downloads/collinear/shape_collinear_simple')

# Example usage
shapefile_path = '/home/vitor/Downloads/collinear/shape_collinear.shp'
clean_shapefile(shapefile_path)