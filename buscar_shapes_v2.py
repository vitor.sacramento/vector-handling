import os
import shutil
import fnmatch
import time

start_time = time.time()

folder_paths = [r'/home/vitor/Documentos/Importacao_legado/brasil/'
]

# Set the path to the destination folder
dest_folder_path = r'/home/vitor/Documentos/Importacao_legado/aprt_limite/'

# Set the search pattern
pattern = '*aprt_limite*'
pattern_to_exclude = 'app'
extension = '*.shp'

# Initialize a counter to keep track of the index
index = 0

# Iterate through the folders
for folder_path in folder_paths:
    # Walk through the folder and its subfolders
    for root, dirs, files in os.walk(folder_path):
        # Iterate through the files in the current folder
        for file in files:
            # Check if the file is a shapefile
            if fnmatch.fnmatch(str.lower(file), extension) and not fnmatch.fnmatch(str.lower(file), pattern_to_exclude) and fnmatch.fnmatch(str.lower(file), pattern):
                # Get the base name of the shapefile (without the extension)
                base_name = os.path.splitext(file)[0]
                # Construct the full path to the shapefile
                file_path = os.path.join(root, file)
                # Construct the destination path for the shapefile
                dest_path = os.path.join(dest_folder_path, str(index) + '_' + file)
                if not os.path.exists(dest_folder_path):
                    os.makedirs(dest_folder_path)
                # Copy the shapefile to the destination folder
                shutil.copy(file_path, dest_path)
                # Iterate through the remaining files in the current folder
                print(file_path)
                for f in files:
                    # Check if the file is associated with the shapefile
                    if f.startswith(base_name) and f != file:
                        # Construct the full path to the associated file
                        file_path = os.path.join(root, f)
                        # Construct the destination path for the associated file
                        dest_path = os.path.join(dest_folder_path, str(index) + '_' + f)
                        # Copy the associated file to the destination folder
                        shutil.copy(file_path, dest_path)
                # Increment the counter
                index += 1

# Calculate the elapsed time
elapsed_time = time.time() - start_time
print(f"Elapsed time: {elapsed_time:.2f} seconds")