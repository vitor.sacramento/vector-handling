import shapefile
import os
import fnmatch
import geopandas as gpd

src_shapefiles = os.path.abspath(r'/home/vitor/Downloads/sitio_pinheiro/')

to_erase_files = []

pattern = '*.shp'

for dirpath, dirnames, files in os.walk(src_shapefiles):
    for filename in files:
            if fnmatch.fnmatch(str.lower(filename), pattern):
                shapefile = os.path.join(dirpath, filename)
                to_erase_files.append(shapefile)

def is_corrupt_shapefile(file):
    try:
        gpd.read_file(file)
        return False
    except Exception as e:
        print(f"{file} is corrupt: {e}")
        return True

for file in to_erase_files:
    if is_corrupt_shapefile(file):
        os.remove(file)
        to_erase_files.remove(file)