import os
import fnmatch
from osgeo import ogr, osr
import logging
from tkinter.filedialog import askdirectory

src_shapefiles = askdirectory()
shapefiles = []

logging.basicConfig(filename=src_shapefiles+'/error_class.log', level=logging.ERROR)

for dirpath, dirnames, files in os.walk(src_shapefiles):
    for filename in files:
        if fnmatch.fnmatch(str.lower(filename), '*class*') and fnmatch.fnmatch(filename, '*.shp'):
            shapefile = os.path.join(dirpath, filename)
            shapefiles.append(shapefile)


# Define the WGS84 spatial reference
projection = osr.SpatialReference()
projection.ImportFromEPSG(4326)

# Loop through the list of src_shapefiles
for shapefile in shapefiles:
    try:
        src_ds = ogr.Open(shapefile)
        if src_ds is None:
            print(f"Could not open {shapefile}")
            continue
        src_layer = src_ds.GetLayer()     
    except AttributeError as e:
        print(f"Error processing {shapefile}: {e}")
    
    # Create the new directory
    dst_directory = os.path.join(os.path.dirname(shapefile), "novos")
    if not os.path.exists(dst_directory):
        os.makedirs(dst_directory)

    # Path to the new shapefile
    dst_shapefile = os.path.join(os.path.dirname(shapefile),"novos","aprt_class_" + os.path.basename(shapefile))

    # Create the new shapefile
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = driver.CreateDataSource(dst_shapefile)

    # Define the new shapefile's spatial reference
    dst_sr = projection

    # Create the new shapefile's layer
    dst_layer = dst_ds.CreateLayer("limite", dst_sr, ogr.wkbPolygon)

    # Add the fields to the new shapefile's layer
    field_descricao = ogr.FieldDefn("descricao", ogr.OFTString)
    field_descricao.SetWidth(35)
    try:
        dst_layer.CreateField(field_descricao)
    except AttributeError as e:
        print(e)

    field_fonte = ogr.FieldDefn("fonte", ogr.OFTString)
    field_fonte.SetWidth(30)
    try:
        dst_layer.CreateField(field_fonte)
    except AttributeError as e:
        print(e)

    field_sensor = ogr.FieldDefn("sensor", ogr.OFTString)
    field_sensor.SetWidth(20)
    try:
        dst_layer.CreateField(field_sensor)
    except AttributeError as e:
        print(e)

    field_data = ogr.FieldDefn("data", ogr.OFTString)
    field_data.SetWidth(10)
    try:
        dst_layer.CreateField(field_data)
    except AttributeError as e:
        print(e)

    # Copy the features from the source shapefile to the new shapefile
    try:
        for src_feature in src_layer:
            if src_feature is not None:
                # Create a new destination feature for each source feature
                dst_feature = ogr.Feature(dst_layer.GetLayerDefn())

                fields_to_copy = ["teste","DESCRIÇÃO","CLASSIFICA","DESCRICAO","classe"]
                success = False
                for field in fields_to_copy:
                    try:
                        value = src_feature.GetField(field)
                        if isinstance(value, str):
                            dst_feature.SetField("descricao", value)
                            success = True
                        else:
                            print("Not a String")
                            continue       
                    except Exception as e:
                        continue
                if not success:
                    logging.error(str(e))

                try:
                    src_geometry = src_feature.GetGeometryRef()
                    if src_geometry is not None:
                        dst_geometry = src_geometry.Clone()
                        dst_geometry.TransformTo(dst_sr) # Transform the geometry to WGS84
                        dst_geometry.SwapXY() # Invert the x and y axis
                        dst_feature.SetGeometry(dst_geometry)
                    else:
                        print("Warning: Invalid geometry, skipping.")
                        continue

                    dst_layer.CreateFeature(dst_feature)
                except AttributeError as e:
                    logging.error(str(e))
                    continue
    except Exception as e:
        logging.error(str(e))