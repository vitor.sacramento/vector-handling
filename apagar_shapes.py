import os
import shapefile
from tkinter.filedialog import askdirectory
import time

# Set the workspace
workspace = askdirectory()

# Track the start time
start_time = time.time()

# Get a list of all the shapefiles in the workspace
try:
    shapefiles = [filename for filename in os.listdir(workspace) if filename.endswith(".shp")]
except FileNotFoundError as e:
    print(f"Directory not found: ", e)
    exit()

# Track the deleted files
deleted_files = []

# Iterate through the list of shapefiles
for shapefile_name in shapefiles:
    # Open the shapefile using the shapefile library
    shapefile_path = os.path.join(workspace, shapefile_name)
    try:
        with shapefile.Reader(shapefile_path) as shapefile_reader:
            # Check if the shapefile has zero features
            if shapefile_reader.numRecords == 0:
                # Delete the .dbf, .shp, and .shx files for the shapefile
                for ext in ('.shp', '.shx', '.dbf', '.prj', '.qpj', '.cpg', '.shp.xml'):
                    file_path = os.path.join(workspace, shapefile_name[:-4] + ext)
                    if os.path.exists(file_path):
                        os.remove(file_path)
                        deleted_files.append(file_path)
    except shapefile.ShapefileException:
        print(f"Error reading shapefile: {shapefile_name}")
    except Exception as e:
        print(f"Error deleting files for shapefile {shapefile_name}: {e}")

# Print the list of deleted files
if deleted_files:
    print("Deleted files:")
    for file_path in deleted_files:
        print(file_path)
else:
    print("No files were deleted.")

# Calculate the elapsed time
elapsed_time = time.time() - start_time
print(f"Elapsed time: {elapsed_time:.2f} seconds")