import os
from osgeo import ogr, osr
from tkinter.filedialog import askdirectory
from tkinter.filedialog import askopenfilename
from tkinter.simpledialog import askstring
import geopandas as gpd

ogr.UseExceptions()

# Open the input shapefile
input_shapefile = askopenfilename(
    title='Selecione o layer de entrada',
#    filetypes=[('Shapefile', '*.shp')],
    initialdir='/home/vitor/Documentos/'
    )

if input_shapefile is None:
    exit()

gdf = gpd.read_file(input_shapefile)
print(gdf)

input_datasource = ogr.Open(input_shapefile)
input_layer = input_datasource.GetLayer()

field_to_split = askstring(prompt='Digite o campo a ser dividido',title=None)

if field_to_split is None:
    exit()

prefix = askstring(prompt='Digite o prefixo do arquivo de saída',title=None)

if prefix is None:
    exit()

# Define the WGS84 spatial reference
projection = osr.SpatialReference()
projection.ImportFromEPSG(4326)

# Get the input shapefile directory and filename
input_directory, input_filename = os.path.split(input_shapefile)

# Get the output directory
output_directory = askdirectory(
    title='Selecione a pasta de saída',
    initialdir='/home/vitor/Documentos'
    )

if output_directory is None:
    exit()

# Get a list of unique attribute values
unique_values = set()
for feature in input_layer:
    unique_values.add(feature.GetField(field_to_split))

# Create output shapefiles for each unique attribute value in the chosen directory
for value in unique_values:
    output_shapefile = os.path.join(output_directory, prefix+'_'+f'{field_to_split}_{value}.shp')
    if os.path.exists(output_shapefile):
        os.remove(output_shapefile)
    output_driver = ogr.GetDriverByName('ESRI Shapefile')
    output_datasource = output_driver.CreateDataSource(output_shapefile)
    output_layer = output_datasource.CreateLayer(output_shapefile, projection, geom_type=ogr.wkbPolygon)

    # Define output layer's fields explicitly based on input layer's fields
    input_layer_defn = input_layer.GetLayerDefn()
    for i in range(input_layer_defn.GetFieldCount()):
        field_defn = input_layer_defn.GetFieldDefn(i)
        output_layer.CreateField(field_defn)

    # Copy features to the output layer for the current attribute value
    input_layer.SetAttributeFilter(f"{field_to_split} = '{value}'")
    for input_feature in input_layer:
        output_feature = ogr.Feature(output_layer.GetLayerDefn())
        output_feature.SetGeometry(input_feature.GetGeometryRef())
        for i in range(input_feature.GetFieldCount()):
            output_feature.SetField(i, input_feature.GetField(i))
        output_layer.CreateFeature(output_feature)

    # Cleanup
    output_datasource = None
    output_datasource = None
    output_layer = None