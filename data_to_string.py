import json
from datetime import datetime
from tkinter.filedialog import askopenfilename

input_file = askopenfilename(title='Selecione o layer')

# Load the GeoJSON file
with open(input_file, 'r') as f:
    geojson_data = json.load(f)

# Function to convert datetime to string
def convert_datetime(field_value):
    if isinstance(field_value, datetime):
        return field_value.strftime('%Y-%m-%d %H:%M:%S')
    else:
        return field_value

# Loop through all features and properties
for feature in geojson_data['features']:
    for key in feature['properties']:
        feature['properties'][key] = convert_datetime(feature['properties'][key])

# Save the updated GeoJSON file
with open('updated_geojson_file.geojson', 'w') as f:
    json.dump(geojson_data, f)