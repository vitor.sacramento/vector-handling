import os
import shutil
import fnmatch
import threading
from collections import deque
import time

start_time = time.time()

folder_paths = deque([r'/mnt/share/'
])

# Set the path to the destination folder
dest_folder_path = r'/home/vitor/Documentos/Importacao_legado/aprt_class/'

# Set the search pattern
pattern = '*aprt_limite*'
pattern_to_exclude = '*app*'
extension = '*.shp'

# Initialize a dictionary to keep track of copied files
copied_files = {}
index = 0
# Define a function to search for and copy shapefiles
def copy_shapefiles():
    global index
    while folder_paths:
        folder_path = folder_paths.popleft()
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                if fnmatch.fnmatch(str.lower(file), extension):
                    if not fnmatch.fnmatch(str.lower(file), pattern_to_exclude):
                        if fnmatch.fnmatch(str.lower(file), pattern):
                            base_name = os.path.splitext(file)[0]
                            file_path = os.path.join(root, file)
                            dest_path = os.path.join(dest_folder_path, str(index) + '_' + file)
                            if file_path not in copied_files:
                                if not os.path.exists(dest_folder_path):
                                    os.makedirs(dest_folder_path)
                                shutil.copy(file_path, dest_path)
                                print(file_path)
                                copied_files[file_path] = dest_path
                                for f in files:
                                    if f.startswith(base_name) and f != file:
                                        file_path = os.path.join(root, f)
                                        dest_path = os.path.join(dest_folder_path, str(index) + '_' + f)
                                        if file_path not in copied_files:
                                            shutil.copy(file_path, dest_path)
                                            copied_files[file_path] = dest_path
                                index += 1

# Define a list to store threads
threads = []

# Define the number of threads to use
num_threads = os.cpu_count()

# Create and start the threads
for i in range(num_threads):
    t = threading.Thread(target=copy_shapefiles)
    threads.append(t)
    t.start()

# Wait for all threads to finish
for t in threads:
    t.join()

# Calculate the elapsed time
elapsed_time = time.time() - start_time
print(f"Elapsed time: {elapsed_time:.2f} seconds")