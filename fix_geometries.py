import geopandas as gpd
from tkinter.filedialog import askopenfilenames
from tqdm import tqdm 

input_files = askopenfilenames(title='Selecione o layer de entrada')

for file in tqdm(input_files):
    gdf = gpd.read_file(file)
    type = gdf.geom_type
    if type[0] == 'Polygon' or type[0] == 'MultiPolygon':
        gdf['geometry'] = gdf.buffer(0)
        print('Buffer 0')
    else:
        pass
    print('Dropping ' + str(gdf.geometry.isna().sum()) + ' nulls.')
    gdf = gdf.dropna(subset=['geometry'])
    gdf.to_file(file)