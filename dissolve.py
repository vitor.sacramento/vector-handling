import geopandas as gpd
import os
import fnmatch
from tkinter.filedialog import askdirectory

src_shapefiles = askdirectory()
shapefiles = []

for dirpath, dirnames, files in os.walk(src_shapefiles):
    for filename in files:
        if fnmatch.fnmatch(str.lower(filename), '*drenagem*') and fnmatch.fnmatch(filename, '*.shp') and not fnmatch.fnmatch(filename, 'FP'):
            shapefile = os.path.join(dirpath, filename)
            shapefiles.append(shapefile)

# Loop through the input file paths
for shapefile in shapefiles:
    # Read the input file
    input_file = gpd.read_file(shapefile)

    # Dissolve the input file based on the specified fields
    dissolved = input_file.dissolve(by='tipo')

    # Construct the output file path by appending '_dissolve' to the input file path
    output_path = os.path.splitext(shapefile)[0] + '_dissolved' + '.shp'

    # Save the dissolved shapefile to the output file path
    dissolved.to_file(output_path)

    input_file = None
    dissolved = None