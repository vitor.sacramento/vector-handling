import geopandas as gpd
import fiona
from tkinter.filedialog import askopenfilename
from tkinter.simpledialog import askstring
import os

# Select the input file
input_file = askopenfilename(title='Selecione a camada de entrada')

# Select the name and location for the output file
output_file = askstring(prompt='Selecione o nome do arquivo de saída',title=None)
output_file = os.path.splitext(output_file)[0] + '.gpkg'
output_file = os.path.join(os.path.dirname(input_file), output_file)

# Read the input data into a geopandas dataframe
data = gpd.read_file(input_file)

# Drop any columns with dtype 'object'
data = data.select_dtypes(exclude=['object'])

# Get the schema of the geopackage file
schema = data.schema.copy()

# Add the layer to the geopackage
with fiona.open(output_file, 'w', layer=data.name, driver='GPKG', crs=data.crs, schema=schema) as out_file:
    data_iter = data.iterfeatures()
    out_file.writerecords(data_iter)