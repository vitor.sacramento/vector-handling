from osgeo import ogr
import os
from tkinter.filedialog import askdirectory

# Set the directory containing the input shapefiles
shapefile_dir = askdirectory(title='Selecione a pasta de entrada')

# Get a list of all the shapefiles in the directory
shapefiles = [f for f in os.listdir(shapefile_dir) if f.endswith('.shp')]

# Loop over each shapefile and convert it to KML
for shapefile in shapefiles:
    # Open the input shapefile
    input_shp = ogr.Open(os.path.join(shapefile_dir, shapefile))

    # Get the layer from the Shapefile
    layer = input_shp.GetLayer()

    # Create the output KML file
    output_kml = ogr.GetDriverByName('KML').CreateDataSource(os.path.join(shapefile_dir, os.path.splitext(shapefile)[0])+".kml")

    # Create the KML layer
    kml_layer = output_kml.CreateLayer('output', layer.GetSpatialRef(), ogr.wkbPolygon)

    # Copy the attribute table schema from the input layer to the output layer
    for feature in layer:
        # Check if the feature has a geometry
        if feature.GetGeometryRef() is None:
            print("Skipping feature with missing geometry")
            continue

        # Check if the feature geometry type is supported in KML
        geom_type = feature.GetGeometryRef().GetGeometryType()
        if geom_type not in [ogr.wkbPolygon, ogr.wkbMultiPolygon]:
            print(f"Skipping feature with unsupported geometry type: {geom_type}")
            continue

        kml_feature = ogr.Feature(kml_layer.GetLayerDefn())
        kml_feature.SetGeometry(feature.GetGeometryRef().Clone())

        # Copy the attribute values from the input feature to the output feature
        for i in range(feature.GetFieldCount()):
            value = feature.GetField(i)
            kml_feature.SetField(i, value)

        kml_layer.CreateFeature(kml_feature)
        kml_feature = None