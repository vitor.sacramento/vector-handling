import os
import fnmatch
from osgeo import ogr, osr
from tkinter.filedialog import askdirectory

src_shapefiles = askdirectory()
shapefiles = []

for dirpath, dirnames, files in os.walk(src_shapefiles):
    for filename in files:
        if fnmatch.fnmatch(str.lower(filename), '*rio*'):
            if fnmatch.fnmatch(filename, '*.shp'):
                shapefile = os.path.join(dirpath, filename)
                shapefiles.append(shapefile)

# Define the WGS84 spatial reference
projection = osr.SpatialReference()
projection.ImportFromEPSG(4326)

# Loop through the list of src_shapefiles
for shapefile in shapefiles:
    try:
        src_ds = ogr.Open(shapefile)
        if src_ds is None:
            print(f"Could not open {shapefile}")
            continue
        src_layer = src_ds.GetLayer()     
    except AttributeError as e:
        print(f"Error processing {shapefile}: {e}")
    
    # Create the new directory
    dst_directory = os.path.dirname(shapefile) + "/novos"
    if not os.path.exists(dst_directory):
        os.makedirs(dst_directory)

    # Path to the new shapefile
    dst_shapefile = os.path.dirname(shapefile) +"/novos" +"/"+ "aprt_drenagem.shp"
    # Create the new shapefile
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = driver.CreateDataSource(dst_shapefile)

    # Define the new shapefile's spatial reference
    dst_sr = projection

    # Create the new shapefile's layer
    dst_layer = dst_ds.CreateLayer("limite", dst_sr, ogr.wkbLineString)

    # Add the fields to the new shapefile's layer
    field_largura = ogr.FieldDefn("largura_m", ogr.OFSTInt16)
    field_largura.SetWidth(4)
    try:
        dst_layer.CreateField(field_largura)
    except AttributeError as e:
        print(e)

    field_nome = ogr.FieldDefn("nome", ogr.OFTString)
    field_nome.SetWidth(30)
    try:
        dst_layer.CreateField(field_nome)
    except AttributeError as e:
        print(e)

    field_tipo = ogr.FieldDefn("tipo", ogr.OFTString)
    field_tipo.SetWidth(20)
    try:
        dst_layer.CreateField(field_tipo)
    except AttributeError as e:
        print(e)

    # Copy the features from the source shapefile to the new shapefile
    try:
        for src_feature in src_layer:
            if src_feature is not None:
                # Create a new destination feature for each source feature
                dst_feature = ogr.Feature(dst_layer.GetLayerDefn())

                fields_to_copy = ["largura","LARGURA"]

                try:
                    for field in fields_to_copy:
                        try:
                            value = src_feature.GetField(str(field))
                            if isinstance(value, str):
                                dst_feature.SetField("largura_m", value)
                            else:
                                print("Not a String")
                                continue
                        except Exception as e:
                            print(f"An unexpected error occurred while setting the {field} field value: {str(e)}")
                            continue
                except Exception as e:
                    print(e)

                fields_to_copy = ["nome","NOME"]

                try:
                    for field in fields_to_copy:
                        try:
                            value = src_feature.GetField(str(field))
                            if isinstance(value, str):
                                dst_feature.SetField("descricao", value)
                            else:
                                print("Not a String")
                                continue
                        except AttributeError as e:
                            print(f"An error occurred while setting the {field} field value: {str(e)}")
                        except Exception as e:
                            print(f"An unexpected error occurred while setting the {field} field value: {str(e)}")
                            continue
                except Exception as e:
                    print(e)

                dst_feature.SetField("largura_m", 9)
                dst_feature.SetField("tipo", 'perene')
                try:
                    src_geometry = src_feature.GetGeometryRef()
                    if src_geometry is not None:
                        dst_geometry = src_geometry.Clone()
                        dst_geometry.TransformTo(dst_sr) # Transform the geometry to WGS84
                        dst_geometry.SwapXY() # Invert the x and y axis
                        dst_feature.SetGeometry(dst_geometry)
                    else:
                        print("Warning: Geometria inválida, pulando.")
                        continue

                    dst_layer.CreateFeature(dst_feature)

                except AttributeError as e:
                    print("Error:", str(e))
                    continue
    except Exception as e:
        print(e)
    # Clean up
    src_ds = None
    dst_ds = None