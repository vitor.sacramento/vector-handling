import os
import exifread
from shapely.geometry import Point
from tkinter.filedialog import askdirectory
from tkinter.filedialog import asksaveasfilename
import geopandas as gpd

image_folder = askdirectory(title='Select the input folder')
output_shapefile = asksaveasfilename(title='Select the output folder', filetypes=[("Shapefile", "*.shp")])

def get_gps_coordinates(image_path):
    with open(image_path, 'rb') as image_file:
        tags = exifread.process_file(image_file, details=False)
        latitude_ref = tags.get('GPS GPSLatitudeRef')
        longitude_ref = tags.get('GPS GPSLongitudeRef')
        latitude = tags.get('GPS GPSLatitude')
        longitude = tags.get('GPS GPSLongitude')

        if latitude and longitude and latitude_ref and longitude_ref:
            lat_deg = latitude.values[0].num / latitude.values[0].den
            lat_min = latitude.values[1].num / latitude.values[1].den / 60
            lat_sec = latitude.values[2].num / latitude.values[2].den / 3600
            lat = (lat_deg + lat_min + lat_sec) * (1 if latitude_ref.values == 'N' else -1)

            lon_deg = longitude.values[0].num / longitude.values[0].den
            lon_min = longitude.values[1].num / longitude.values[1].den / 60
            lon_sec = longitude.values[2].num / longitude.values[2].den / 3600
            lon = (lon_deg + lon_min + lon_sec) * (1 if longitude_ref.values == 'E' else -1)

            return lon, lat
        else:
            return None
        
def create_dataframe_from_files(output_shapefile, coordinates_list, file_names_list):
    if coordinates_list:
        points = [Point(lon, lat) for lon, lat in coordinates_list]

        data = {'geometry': points, 'longitude': [lon for lon, lat in coordinates_list],
                'latitude': [lat for lon, lat in coordinates_list], 'descricao': file_names_list}
        gdf = gpd.GeoDataFrame(data, crs='EPSG:4326')

        gdf.to_file(output_shapefile)

        print("Shapefile created.")
    else:
        print("No coordinates found")


def write_shape_file():
    image_files = [f for f in os.listdir(image_folder) if f.lower().endswith(('.jpg', '.jpeg', '.png', '.gif'))]
    coordinates_list = []
    file_names_list = []

    for image_file in image_files:
        image_path = os.path.join(image_folder, image_file)
        coordinates = get_gps_coordinates(image_path)
        if coordinates:
            coordinates_list.append(coordinates)
            file_names_list.append(image_file)
    create_dataframe_from_files(output_shapefile, coordinates_list, file_names_list)

if __name__ == "__main__":  
    write_shape_file()