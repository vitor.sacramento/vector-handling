from tkinter.filedialog import askopenfilename
from tkinter.simpledialog import askstring
from osgeo import ogr

ogr.UseExceptions()

def check_bounds():
# select the input layer
    input_layer = askopenfilename(title='Select the input layer')
    # open the data source
    dataSource = ogr.Open(input_layer)
    # get the layer
    layer = dataSource.GetLayer()
    # initialize variables for the bounding box
    minX, maxX, minY, maxY = float('inf'), float('-inf'), float('inf'), float('-inf')

# iterate over the features
    for feature in layer:
        # get the geometry
        geometry = feature.GetGeometryRef()
        
        # get the bounding box
        bbox = geometry.GetEnvelope()
        
        # update the variables for the bounding box
        minX = min(minX, bbox[0])
        maxX = max(maxX, bbox[1])
        minY = min(minY, bbox[2])
        maxY = max(maxY, bbox[3])

    # print the bounding box
    print((minX, maxX, minY, maxY))

    if minX < -180 or maxX > 180 or minY < -90 or maxY > 90:
        print('Polygon out of bounds for WGS 84')
    else:
        print('Polygon inbounds for WGS 84')

    # recursive function
    recurse = askstring(prompt='Do you wish to check for another layer? y/n',title=None)
    if recurse != 'n':
        check_bounds()
    else:
        exit()

check_bounds()