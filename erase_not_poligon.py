import geopandas as gpd
import os

# Set the directory where the shapefiles are located
directory = r'/home/vitor/Documentos/Importacao_legado/aprt_limite/'

# Loop through all the files in the directory
for filename in os.listdir(directory):
    if filename.endswith('.shp'):
        try:
            # Read the shapefile using Geopandas
            gdf = gpd.read_file(os.path.join(directory, filename))
            
            # Check if the geometry type is polygon or multipolygon
            if gdf.geom_type[0] not in ['Polygon', 'MultiPolygon']:
                # If it's not a polygon or multipolygon, remove all associated files
                for ext in ['.shp', '.shx', '.dbf', '.prj', '.cpg']:
                    file_path = os.path.join(directory, os.path.splitext(filename)[0] + ext)
                    if os.path.exists(file_path):
                        os.remove(file_path)
                        print(f'Removed {file_path}')
        except Exception as e:
            # Print an error message if there was an error reading the file
            print(f'Error reading file {filename}: {str(e)}')
            # If the file couldn't be read, remove all associated files
            for ext in ['.shp', '.shx', '.dbf', '.prj', '.cpg']:
                file_path = os.path.join(directory, os.path.splitext(filename)[0] + ext)
                if os.path.exists(file_path):
                    os.remove(file_path)
                    print(f'Removed {file_path}')